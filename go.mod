module bitbucket.org/uwaplmlf2/mlf2db

go 1.13

require (
	bitbucket.org/uwaplmlf2/couchdb v0.7.1
	github.com/mitchellh/go-homedir v1.1.0
	github.com/pkg/errors v0.9.1
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.6.3
)
