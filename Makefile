DATE    ?= $(shell date +%FT%T%z)
VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2> /dev/null || \
			cat $(CURDIR)/.version 2> /dev/null || echo v0)

GOBUILD := go build
APP := mlf2db

all: $(APP)

dep:
	@go get -v -d ./...

$(APP): dep
	@$(GOBUILD) -v -o $@ \
	  -tags release \
	  -ldflags '-s -X main.Version=$(VERSION) -X main.BuildDate=$(DATE)'

clean:
	@rm -f $(APP)
