# MLF2 Database Utility

The program `mlf2db` is used to add floats, missions, and users to the
web-based MLF2 database. A YAML format configuration file
`$HOME/.config/mlf2db/config.yaml` is read to obtain the database log-in credentials.

## Configuration File

There are separate sections for each database on the server.

``` yaml
---
markers: "/Users/mike/float-markers"
host: "mlf2data.apl.uw.edu:443"
user: USERNAME
password: PASSWORD
databases:
  mlf2:
    name: mlf2db
  rudics:
    name: rudics
  users:
    name: "_users"
    user: admin
    password: ADMINPASSWORD

```

*Markers* specifies a local directory containing Google Maps icons for
different float IDs. A zipfile of these icons can be downloaded from the
*Downloads* section of this repository:

https://bitbucket.org/uwaplmlf2/mlf2db/downloads/float-markers.zip

## Usage

The program is comprised of a series of sub-commands.

``` shellsession
$ mlf2db --help
MLF2 database utility

Usage:
  mlf2db [command]

Available Commands:
  addfloat    Add a new float
  addmission  Add a new mission (deployment)
  adduser     Add a user account
  help        Help about any command
  moduser     Modify a user account
  showuser    Display user account information

Flags:
      --config string   config file (default is $HOME/.config/mlf2db/config.yaml)
  -h, --help            help for mlf2db
      --version         version for mlf2db

Use "mlf2db [command] --help" for more information about a command.
```

Help is available for each sub-command:

``` shellsession
$ mlf2db addfloat --help
Add a new float entry specifying the float ID and the IMEI number
of its Iridium modem (aka Iridium Serial Number). This command also adds a
corresponding entry to the RUDICS database.

If a float "markers" directory is specified, it is searched for a image file named
floatN.png, where N is the float ID. If found, this will be uploaded to the database
and serve as an icon in the Google Maps display.

Usage:
  mlf2db addfloat id IMEI [flags]

Flags:
  -g, --group string     Float group name
  -h, --help             help for addfloat
  -m, --markers string   Float markers directory

Global Flags:
      --config string   config file (default is $HOME/.config/mlf2db/config.yaml)
```

``` shellsession
$ mlf2db addmission --help
Adding a new deployment requires specifing an ID which must not
contain spaces, a start time in RFC3339 format, and a length in days. The
"desc" option can be used to provide a more descriptive name:

  mlf2db addmission --desc="SPURS2 2018 Deployment" --floats=81,82 spurs2 2018-09-01T15:00:00 60

Usage:
  mlf2db addmission id starttime duration [flags]

Flags:
  -d, --desc string     Deployment description
  -f, --floats string   Comma separated list of float IDs
  -h, --help            help for addmission

Global Flags:
      --config string   config file (default is $HOME/.config/mlf2db/config.yaml)
```

``` shellsession
$ mlf2db adduser --help
Add a user account

Usage:
  mlf2db adduser name email [flags]

Flags:
  -h, --help              help for adduser
  -p, --password string   User password
  -r, --roles string      Comma separate list of roles

Global Flags:
      --config string   config file (default is $HOME/.config/mlf2db/config.yaml)
```

``` shellsession
$ mlf2db moduser --help
Modify a user account

Usage:
  mlf2db moduser name [flags]

Flags:
  -a, --append            Append new roles
  -h, --help              help for moduser
  -p, --password string   User password
  -r, --roles string      Comma separate list of roles

Global Flags:
      --config string   config file (default is $HOME/.config/mlf2db/config.yaml)
```

``` shellsession
$ mlf2db showuser --help
Display user account information

Usage:
  mlf2db showuser name [flags]

Flags:
  -h, --help   help for showuser

Global Flags:
      --config string   config file (default is $HOME/.config/mlf2db/config.yaml)
```
