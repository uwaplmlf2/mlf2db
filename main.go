// Mlf2db adds new entries to the MLF2 database.
package main

import "bitbucket.org/uwaplmlf2/mlf2db/cmd"

var Version = "dev"
var BuildDate = "unknown"

func main() {
	cmd.Execute(Version)
}
