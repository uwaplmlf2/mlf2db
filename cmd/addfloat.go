package cmd

import (
	"fmt"
	"net/url"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var floatGroup string

// addfloatCmd represents the addfloat command
var addfloatCmd = &cobra.Command{
	Use:   "addfloat id IMEI",
	Short: "Add a new float",
	Long: `Add a new float entry specifying the float ID and the IMEI number
of its Iridium modem (aka Iridium Serial Number). This command also adds a
corresponding entry to the RUDICS database.

If a float "markers" directory is specified, it is searched for a image file named
floatN.png, where N is the float ID. If found, this will be uploaded to the database
and serve as an icon in the Google Maps display.`,
	Args:         cobra.ExactArgs(2),
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		dbcfg := viper.GetStringMapString("databases.mlf2")
		if !viper.IsSet("databases.mlf2.user") {
			dbcfg["user"] = viper.GetString("user")
			dbcfg["password"] = viper.GetString("password")
		}

		u := url.URL{}
		u.Host = viper.GetString("host")
		u.User = url.UserPassword(dbcfg["user"], dbcfg["password"])
		if strings.HasSuffix(u.Host, ":443") {
			u.Scheme = "https"
		} else {
			u.Scheme = "http"
		}

		db, err := openDatabase(u, dbcfg["name"])
		if err != nil {
			return err
		}

		floatid, err := strconv.Atoi(args[0])
		if err != nil {
			return err
		}

		err = addFloatRudics(floatid, args[1])
		if err != nil {
			return errors.Wrap(err, "RUDICS entry error")
		}

		var fname string
		if dname := viper.GetString("markers"); dname != "" {
			fname = filepath.Join(dname, fmt.Sprintf("float%d.png", floatid))
		}
		id, err := addFloat(db, floatid, args[1], floatGroup, fname)
		if err != nil {
			return err
		}
		fmt.Printf("Add float entry %q\n", id)
		return nil
	},
}

func init() {
	rootCmd.AddCommand(addfloatCmd)

	addfloatCmd.Flags().StringP("markers", "m", "", "Float markers directory")
	addfloatCmd.Flags().StringVarP(&floatGroup, "group", "g", "",
		"Float group name")
	viper.BindPFlag("markers", addfloatCmd.Flags().Lookup("markers"))
	viper.BindEnv("markers")
}

func addFloatRudics(floatid int, imei string) error {
	dbcfg := viper.GetStringMapString("databases.rudics")
	if !viper.IsSet("databases.rudics.user") {
		dbcfg["user"] = viper.GetString("user")
		dbcfg["password"] = viper.GetString("password")
	}

	u := url.URL{}
	u.Host = viper.GetString("host")
	u.User = url.UserPassword(dbcfg["user"], dbcfg["password"])
	if strings.HasSuffix(u.Host, ":443") {
		u.Scheme = "https"
	} else {
		u.Scheme = "http"
	}

	db, err := openDatabase(u, dbcfg["name"])
	if err != nil {
		return err
	}
	_, err = addRudics(db, floatid, imei)
	return err
}
