package cmd

import (
	"crypto/rand"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"io"
	"net/url"
	"os"
	"time"

	"bitbucket.org/uwaplmlf2/couchdb"
	"github.com/pkg/errors"
)

type RudicsDevice struct {
	ID       string `json:"_id"`
	Rev      string `json:"_rev,omitempty"`
	Name     string `json:"name"`
	Imei     string `json:"imei"`
	Type     string `json:"type"`
	Owner    string `json:"owner,omitempty"`
	Endpoint string `json:"endpoint,omitempty"`
}

type Mlf2Float struct {
	ID      string `json:"_id"`
	Rev     string `json:"_rev,omitempty"`
	Floatid int    `json:"floatid"`
	Group   string `json:"group,omitempty"`
	Imei    string `json:"imei"`
	Type    string `json:"type"`
}

type Mission struct {
	ID     string `json:"_id"`
	Rev    string `json:"_rev,omitempty"`
	Floats []int  `json:"floats"`
	Name   string `json:"name"`
	Start  string `json:"start"`
	Stop   string `json:"stop"`
	Type   string `json:"type"`
}

type DbUser struct {
	ID       string            `json:"_id"`
	Rev      string            `json:"_rev,omitempty"`
	Type     string            `json:"type"`
	Name     string            `json:"name"`
	Password string            `json:"password_sha"`
	Salt     string            `json:"salt"`
	Scheme   string            `json:"password_scheme"`
	Roles    []string          `json:"roles"`
	Profile  map[string]string `json:"couch.app.profile"`
}

func (u DbUser) String() string {
	return fmt.Sprintf("Name: %s\nId: %s\nRoles: %s\n",
		u.Name, u.ID, u.Roles)
}

func openDatabase(u url.URL, dbname string) (*couchdb.Database, error) {
	db := couchdb.NewDatabase(u.String(), dbname)
	return db, nil
}

func addRudics(rdb *couchdb.Database, float_id int, imei string) (string, error) {
	rd := RudicsDevice{
		ID:   imei,
		Name: fmt.Sprintf("mlf2-%d", float_id),
		Type: "local",
		Imei: imei,
	}
	_, err := rdb.Put(rd.ID, rd)
	return rd.ID, err
}

func addFloat(rdb *couchdb.Database, float_id int, imei, group, icon string) (string, error) {
	var err error
	f := Mlf2Float{
		ID:      fmt.Sprintf("apl.uw.mlf2:%d", float_id),
		Floatid: float_id,
		Imei:    imei,
		Type:    "active",
	}
	rev, err := rdb.Put(f.ID, f)
	if err != nil {
		return "", errors.Wrap(err, "PUT document")
	}

	if icon != "" {
		finfo, err := os.Stat(icon)
		if err == nil {
			file, err := os.Open(icon)
			if err == nil {
				defer file.Close()
				att := couchdb.Attachment{
					Filename:    "marker.png",
					ContentType: "image/png",
					Content:     file,
					Length:      int(finfo.Size()),
				}
				rev, err = rdb.PutAttachment(f.ID, rev, att)
				if err != nil {
					rdb.Delete(f.ID, rev)
				}
			}
		}
	}

	return f.ID, err
}

func hashPassword(password, salt string) string {
	h := sha1.New()
	io.WriteString(h, password)
	io.WriteString(h, salt)
	sum := h.Sum(nil)
	return hex.EncodeToString(sum[0:sha1.Size])
}

func addUser(rdb *couchdb.Database, name, email, password string, roles ...string) (string, error) {
	salt := make([]byte, 16)
	rand.Read(salt)
	enc_salt := hex.EncodeToString(salt)
	u := DbUser{
		ID:       fmt.Sprintf("org.couchdb.user:%s", name),
		Name:     name,
		Password: hashPassword(password, enc_salt),
		Salt:     enc_salt,
		Type:     "user",
		Scheme:   "simple",
	}
	u.Profile = make(map[string]string)
	u.Profile["email"] = email
	u.Roles = make([]string, 0)
	for _, role := range roles {
		u.Roles = append(u.Roles, role)
	}
	_, err := rdb.Put(u.ID, u)
	return u.ID, err
}

func modUser(rdb *couchdb.Database, u DbUser, password string, addRoles bool, roles ...string) error {
	if password != "" {
		salt := make([]byte, 16)
		rand.Read(salt)
		enc_salt := hex.EncodeToString(salt)
		u.Password = hashPassword(password, enc_salt)
		u.Salt = enc_salt
	}

	if len(roles) > 0 {
		if !addRoles {
			u.Roles = make([]string, 0)
		}
		for _, role := range roles {
			u.Roles = append(u.Roles, role)
		}
	}

	_, err := rdb.Put(u.ID, u)
	return err
}

func getUser(rdb *couchdb.Database, name string) (DbUser, error) {
	var u DbUser
	err := rdb.Get(fmt.Sprintf("org.couchdb.user:%s", name), &u)
	return u, err
}

func addMission(rdb *couchdb.Database, name, desc string, start, stop time.Time,
	floats ...int) (string, error) {
	m := Mission{
		ID:     fmt.Sprintf("apl.uw.mlf2:%s", name),
		Name:   desc,
		Start:  start.Format(time.RFC3339),
		Stop:   stop.Format(time.RFC3339),
		Type:   "deployment",
		Floats: floats,
	}
	_, err := rdb.Put(m.ID, m)
	return m.ID, err
}
