package cmd

import (
	"fmt"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	missionDesc   string
	missionFloats string
)

// addmissionCmd represents the addmission command
var addmissionCmd = &cobra.Command{
	Use:   "addmission id starttime duration",
	Short: "Add a new mission (deployment)",
	Long: `Adding a new deployment requires specifing an ID which must not
contain spaces, a start time in RFC3339 format, and a length in days. The
"desc" option can be used to provide a more descriptive name:

  mlf2db addmission --desc="SPURS2 2018 Deployment" --floats=81,82 spurs2 2018-09-01T15:00:00 60
`,
	Args:         cobra.ExactArgs(3),
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		dbcfg := viper.GetStringMapString("databases.mlf2")
		if !viper.IsSet("databases.mlf2.user") {
			dbcfg["user"] = viper.GetString("user")
			dbcfg["password"] = viper.GetString("password")
		}

		u := url.URL{}
		u.Host = viper.GetString("host")
		u.User = url.UserPassword(dbcfg["user"], dbcfg["password"])
		if strings.HasSuffix(u.Host, ":443") {
			u.Scheme = "https"
		} else {
			u.Scheme = "http"
		}

		db, err := openDatabase(u, dbcfg["name"])
		if err != nil {
			return err
		}

		if strings.Contains(args[0], " ") {
			return errors.New("No spaces allowed in mission ID")
		}

		start, err := time.Parse(time.RFC3339, args[1])
		if err != nil {
			return errors.Wrap(err, "Invalid start time")
		}

		days, err := strconv.Atoi(args[2])
		if err != nil {
			return errors.Wrap(err, "Invalid duration")
		}

		floatids := make([]int, 0)
		for _, id := range strings.Split(missionFloats, ",") {
			x, err := strconv.Atoi(id)
			if err != nil {
				return errors.Wrap(err, "Invalid float ID")
			}
			floatids = append(floatids, x)
		}

		id, err := addMission(db, args[0], missionDesc, start,
			start.Add(time.Hour*time.Duration(days)*24),
			floatids...)
		if err != nil {
			return err
		}
		fmt.Printf("Add mission entry %q\n", id)
		return nil

	},
}

func init() {
	rootCmd.AddCommand(addmissionCmd)

	addmissionCmd.Flags().StringVarP(&missionDesc, "desc", "d", "", "Deployment description")
	addmissionCmd.Flags().StringVarP(&missionFloats, "floats", "f", "",
		"Comma separated list of float IDs")
}
