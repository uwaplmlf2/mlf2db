package cmd

import (
	"fmt"
	"net/url"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// userCmd represents the user command
var showuserCmd = &cobra.Command{
	Use:          "showuser name",
	Short:        "Display user account information",
	Args:         cobra.ExactArgs(1),
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		dbcfg := viper.GetStringMapString("databases.users")
		if !viper.IsSet("databases.users.user") {
			dbcfg["user"] = viper.GetString("user")
			dbcfg["password"] = viper.GetString("password")
		}

		u := url.URL{}
		u.Host = viper.GetString("host")
		u.User = url.UserPassword(dbcfg["user"], dbcfg["password"])
		if strings.HasSuffix(u.Host, ":443") {
			u.Scheme = "https"
		} else {
			u.Scheme = "http"
		}

		db, err := openDatabase(u, dbcfg["name"])
		if err != nil {
			return err
		}

		userinfo, err := getUser(db, args[0])
		if err != nil {
			return err
		}
		fmt.Printf("%s", userinfo)

		return nil
	},
}

func init() {
	rootCmd.AddCommand(showuserCmd)
}
