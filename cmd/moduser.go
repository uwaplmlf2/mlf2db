package cmd

import (
	"net/url"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var appendRoles bool

// userCmd represents the user command
var moduserCmd = &cobra.Command{
	Use:          "moduser name",
	Short:        "Modify a user account",
	Args:         cobra.ExactArgs(1),
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		dbcfg := viper.GetStringMapString("databases.users")
		if !viper.IsSet("databases.users.user") {
			dbcfg["user"] = viper.GetString("user")
			dbcfg["password"] = viper.GetString("password")
		}

		u := url.URL{}
		u.Host = viper.GetString("host")
		u.User = url.UserPassword(dbcfg["user"], dbcfg["password"])
		if strings.HasSuffix(u.Host, ":443") {
			u.Scheme = "https"
		} else {
			u.Scheme = "http"
		}

		db, err := openDatabase(u, dbcfg["name"])
		if err != nil {
			return err
		}

		userinfo, err := getUser(db, args[0])
		if err != nil {
			return err
		}

		roles := strings.Split(userRoles, ",")
		return modUser(db, userinfo, userPass, appendRoles, roles...)
	},
}

func init() {
	rootCmd.AddCommand(moduserCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// userCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	moduserCmd.Flags().StringVarP(&userRoles, "roles", "r", "", "Comma separate list of roles")
	moduserCmd.Flags().StringVarP(&userPass, "password", "p", "", "User password")
	moduserCmd.Flags().BoolVarP(&appendRoles, "append", "a", false, "Append new roles")

}
