package cmd

import (
	"fmt"
	"net/url"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var userPass string
var userRoles string

// userCmd represents the user command
var adduserCmd = &cobra.Command{
	Use:          "adduser name email",
	Short:        "Add a user account",
	Args:         cobra.ExactArgs(2),
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		dbcfg := viper.GetStringMapString("databases.users")
		if !viper.IsSet("databases.users.user") {
			dbcfg["user"] = viper.GetString("user")
			dbcfg["password"] = viper.GetString("password")
		}

		u := url.URL{}
		u.Host = viper.GetString("host")
		u.User = url.UserPassword(dbcfg["user"], dbcfg["password"])
		if strings.HasSuffix(u.Host, ":443") {
			u.Scheme = "https"
		} else {
			u.Scheme = "http"
		}

		db, err := openDatabase(u, "_users")
		if err != nil {
			return err
		}
		roles := strings.Split(userRoles, ",")
		id, err := addUser(db, args[0], args[1], userPass, roles...)
		if err != nil {
			return err
		}
		fmt.Printf("Add user entry %q\n", id)
		return nil
	},
}

func init() {
	rootCmd.AddCommand(adduserCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// userCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	adduserCmd.Flags().StringVarP(&userRoles, "roles", "r", "", "Comma separate list of roles")
	adduserCmd.Flags().StringVarP(&userPass, "password", "p", "", "User password")
}
